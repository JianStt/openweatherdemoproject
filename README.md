# OpenWeatherApplication

The project is for demo the integration work with OpenWeather API

# IDE notes

1. The project is using "lombok" annotation, it is suggested that enable "lombok" plugin with your favorite 
   IDE such as Eclipse, STS and Intellij, for trouble shooting please use https://www.baeldung.com/lombok-ide.
   Please contact blue.cuijian@gmail.com if you are running into trouble to mount within your environment.


# Run within your favourite IDE (Eclipse, STS , Intellij)

Using Intellij as example

1. Execute mvn clean install
2. After build successful, execute "WeatherApplication" as "run application"
3. You should see SpringBoot starts at localhost:8080 with following message:
    Tomcat started on port(s): 8080 (http) ....

# Luanch the application with stand alone jar , self library contained FAT jar 
1. After successful build "mvn clean install", there is a FAT jar generated at target/demo.integration-1.0-SNAPSHOT.jar
2. Just type command: "java -jar target/demo.integration-1.0-SNAPSHOT.jar"
3. Your application should start --- no need IDE configuration.


# Have fun to find local city weather! 
   
1. Open web browser at  http://localhost:8080/weatherByCityName   
2. Weather home is up running at a page with weather form should be displayed.
3. Input the city such as 'Vancouver', 'London', 'HongKong', which returns the local weather info
4. Alternativelly you could simply attach the city at the URL sucha as: http://localhost:8080/weatherByCityName?cityName=Toronto
5. It is normal that some input gives no found entry, and message should say it.


#Developer's notes
1. Web page shows null when no city feed returns as this is not required by assignment

#TODO:
1. Ideally there should be auto suggestion when user types a city.
2. Considering get the third party lib that holds geographic city names with country code, or oncloud solution.
3. Docker integration is not in the scope.


#Contact:
Please contact via email blue.cuijian@gmail.com for any questions
 


