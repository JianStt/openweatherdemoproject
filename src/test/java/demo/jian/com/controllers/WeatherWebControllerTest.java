package demo.jian.com.controllers;

import demo.jian.com.model.WeatherWebResult;
import demo.jian.com.services.WeatherService;
import lombok.NoArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherWebControllerTest {

    @Mock
    WeatherService weatherService;

    private WeatherWebController controller;

    @Before
    public void init(){

        WeatherWebResult weatherWebResult = WeatherWebResult.builder().build();
        when(weatherService.checkCityWeatherByName(anyString())).thenReturn(weatherWebResult);
        controller = new WeatherWebController(weatherService);

    }

    @Test
    public void weatherByCityNameTest() {
        assertNotNull(controller.weatherByCityName("vancouver", new MyModel()));
    }



}

@NoArgsConstructor
class MyModel implements Model{

    @Override
    public Model addAttribute(String s, Object o) {
        return null;
    }

    @Override
    public Model addAttribute(Object o) {
        return null;
    }

    @Override
    public Model addAllAttributes(Collection<?> collection) {
        return null;
    }

    @Override
    public Model addAllAttributes(Map<String, ?> map) {
        return null;
    }

    @Override
    public Model mergeAttributes(Map<String, ?> map) {
        return null;
    }

    @Override
    public boolean containsAttribute(String s) {
        return false;
    }

    @Override
    public Map<String, Object> asMap() {
        return null;
    }
}