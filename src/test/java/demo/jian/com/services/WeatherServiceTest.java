package demo.jian.com.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import demo.jian.com.conf.SpringWeatherConfiguration;
import demo.jian.com.conf.WeatherMessagesConfiguration;
import demo.jian.com.model.WeatherWebResult;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceTest {

    @Mock
    private OkHttpClient okHttpClient;

    @Mock
    private ObjectMapper objectMapper;

    private SpringWeatherConfiguration springWeatherConfiguration;

    private WeatherMessagesConfiguration weatherMessagesConfiguration;

    @Mock
    private Call call;

    private WeatherService weatherService;

    @Before
    public void setUp() throws Exception {
        mockNecessaryAttributes();
        weatherService = new WeatherServiceImpl(okHttpClient, objectMapper, springWeatherConfiguration, weatherMessagesConfiguration);
    }

    @After
    public void tearDown() throws Exception {
        objectMapper = null;
        weatherService = null;
    }

    @Test
    public void checkCityWeatherByNameDefaultMessage() {

        WeatherWebResult result = weatherService.checkCityWeatherByName("Vancouver");
        assertNotNull(result);
    }

    @Test
    public void checkCityWeatherByNameMessage() {

        try {
            when(call.execute()).thenThrow(new IOException());
        } catch (IOException e) {
            fail();
        }
        WeatherWebResult result = weatherService.checkCityWeatherByName("Vancouver");
        assertNotNull(result);
    }


    private void mockNecessaryAttributes() {
        springWeatherConfiguration = new SpringWeatherConfiguration();
        springWeatherConfiguration.setApiKey("fakeApiKey");
        springWeatherConfiguration.setEndPointHost("localhost");
        springWeatherConfiguration.setEndPointPath("fake/api/call");
        springWeatherConfiguration.setScheme("http");

        weatherMessagesConfiguration = new WeatherMessagesConfiguration();
        weatherMessagesConfiguration.setDefaultErrorMsg("I am default message");
        weatherMessagesConfiguration.setSuccessfulRetrieved( "hello %s hello %s");
        weatherMessagesConfiguration.setNoCityFound("no city");

        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);

    }

}

