package demo.jian.com.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtcTimeConverterTest {

    @Test
    public void convertUtcUnixSeconds() {
        assertEquals("2019-07-18 12:26:25 UTC", UtcTimeConverter.convertUtcUnixSeconds(1563452785L));
    }

    @Test
    public void convertUtcUnixSecondsLocalTime() {
        assertEquals("05:26 am", UtcTimeConverter.convertUtcUnixSecondsLocalTime(1563452785L, -25200L));
    }

    @Test
    public void timeZoneOffSetComputer() {

        assertEquals("-07:00",UtcTimeConverter.timeZoneOffSetComputer(-25200L));
        assertEquals("+07:00", UtcTimeConverter.timeZoneOffSetComputer(+25200L));
        assertEquals("00:00", UtcTimeConverter.timeZoneOffSetComputer(254343463464364360L));
        assertEquals("00:00", UtcTimeConverter.timeZoneOffSetComputer(-4657433656L));
    }
}