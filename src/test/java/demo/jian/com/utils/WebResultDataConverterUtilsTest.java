package demo.jian.com.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.jian.com.model.WeatherResponse;
import demo.jian.com.model.WeatherWebResult;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class WebResultDataConverterUtilsTest {

    private ObjectMapper objectMapper;
    private WeatherResponse weatherResponse;

    @Before
    public void setUp() throws Exception {

        objectMapper = new ObjectMapper();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("actualPayload.json").getFile());
        try {
            weatherResponse =objectMapper.readValue(file, WeatherResponse.class);
        } catch (IOException e) {
            fail();
        }
        assertNotNull(weatherResponse);
        assertNotNull(weatherResponse.getCod());

    }

    @After
    public void tearDown() throws Exception {
        objectMapper = null;
        weatherResponse = null;
    }

    @Test
    public void populateWeatherResultFromWeatherResponse() {

        WeatherWebResult weatherWebResult = WebResultDataConverterUtils.populateWeatherResultFromWeatherResponse(weatherResponse, "city weather has been successful retrieved");
        assertNotNull(weatherWebResult);
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getTemperatureDisplayInCelsius()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getOverallDescription()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getSunSetInTwleveHoursFormat()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getCityName()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getMessage()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getSunRiseInTwleveHoursFormat()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getTemperatureDisplayInFahrenheit()));
        assertNotNull(StringUtils.isNoneBlank(weatherWebResult.getTodayDate()));
    }

}