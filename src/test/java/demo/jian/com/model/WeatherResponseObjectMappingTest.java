package demo.jian.com.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class WeatherResponseObjectMappingTest {

    ObjectMapper objectMapper;
    @org.junit.Before
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @org.junit.After
    public void tearDown()  {
        objectMapper = null;
    }

    @Test
    public void weatherResponsePayloadTest() {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("actualPayload.json").getFile());
        WeatherResponse weatherResponse = null;
        try {
            weatherResponse =objectMapper.readValue(file, WeatherResponse.class);
        } catch (IOException e) {
            fail();
        }
        assertNotNull(weatherResponse);
        assertNotNull(weatherResponse.getCod());
        assertNotNull(weatherResponse.getTimezone());
        assertNotNull(weatherResponse.getSys());
        assertNotNull(weatherResponse.getSys().getMessage());
        assertNotNull(weatherResponse.getSys().getCountry());
        assertNotNull(weatherResponse.getSys().getSunrise());
        assertNotNull(weatherResponse.getSys().getSunset());
        assertNotNull(weatherResponse.getSys().getId());
        assertNotNull(weatherResponse.getSys().getType());
        assertNotNull(weatherResponse.getName());
        assertNotNull(weatherResponse.getDt());
        assertNotNull(weatherResponse.getMain());
        assertNotNull(weatherResponse.getMain().getTemp());
        assertNotNull(weatherResponse.getMain().getHumidity());
        assertNotNull(weatherResponse.getMain().getPressure());
        assertNotNull(weatherResponse.getMain().getTempMax());
        assertNotNull(weatherResponse.getMain().getTempMin());
        assertNotNull(weatherResponse.getWeather());
        assertNotNull(weatherResponse.getWeather().get(0).getDescription());
        assertNotNull(weatherResponse.getWeather().get(0).getMain());
        assertNotNull(weatherResponse.getWeather().get(0).getIcon());
        assertNotNull(weatherResponse.getWeather().get(0).getId());
        assertNotNull(weatherResponse.getBase());
        assertNotNull(weatherResponse.getClouds());
        assertNotNull(weatherResponse.getClouds().getAll());
        assertNotNull(weatherResponse.getCoord());
        assertNotNull(weatherResponse.getCoord().getLat());
        assertNotNull(weatherResponse.getCoord().getLon());
        assertNotNull(weatherResponse.getId());
        assertNotNull(weatherResponse.getRain());
        assertNotNull(weatherResponse.getRain().getH());
        assertNotNull(weatherResponse.getVisibility());
        assertNotNull(weatherResponse.getWind());
        assertNotNull(weatherResponse.getWind().getDeg());
        assertNotNull(weatherResponse.getWind().getSpeed());

    }

}