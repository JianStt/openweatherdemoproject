package demo.jian.com;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationInitializerTest {


    private ApplicationInitializer applicationInitializer;

    @Before
    public void init(){
        applicationInitializer = new ApplicationInitializer();
    }
    @After
    public void clean(){
        applicationInitializer = null;
    }

    @Test
    public void okHttpClient() {
        assertNotNull(applicationInitializer.okHttpClient());
    }

    @Test
    public void initialObjectMapper() {
        assertNotNull(applicationInitializer.initialObjectMapper());
    }
}