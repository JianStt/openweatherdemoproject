package demo.jian.com;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Initialize spring Beans
 */
@Configuration
public class ApplicationInitializer {

    @Bean(name="okHttpClient")
    public OkHttpClient okHttpClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return builder.build();

    }

    @Bean
    public ObjectMapper initialObjectMapper(){
        return new ObjectMapper(); //Thread safe
    }

}
