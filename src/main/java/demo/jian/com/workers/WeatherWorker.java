package demo.jian.com.workers;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.jian.com.conf.SpringWeatherConfiguration;
import demo.jian.com.exceptions.RestfulCallException;
import demo.jian.com.model.WeatherResponse;
import demo.jian.com.utils.RequestBuildUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.http.HttpStatus;

import java.io.IOException;

/**
 * Weather worker initiated by Weather Service and process job for requests
 */

@Slf4j
@AllArgsConstructor
@Builder
public class WeatherWorker{


    private final ObjectMapper objectMapper;
    private final OkHttpClient okHttpClient;
    private final SpringWeatherConfiguration springWeatherConfiguration;


    /**
     *
     * @param cityName
     * @return
     * @throws IOException
     * @throws RestfulCallException
     */
    public WeatherResponse processRequest(String cityName) throws IOException, RestfulCallException {


        Request request = RequestBuildUtils.buildHttpRequest(cityName, springWeatherConfiguration);

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (null != response && response.isSuccessful()) {
                String responseBody = response.body().string();
                log.debug("Received response body: ", responseBody );

                return objectMapper.readValue(responseBody, WeatherResponse.class);
            }
            throw new RestfulCallException("Unsuccessful response was received ",
                    null== response ? HttpStatus.SC_INTERNAL_SERVER_ERROR: response.code());
        }
    }

}
