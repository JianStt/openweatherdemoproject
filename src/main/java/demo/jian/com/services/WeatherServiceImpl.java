package demo.jian.com.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import demo.jian.com.conf.SpringWeatherConfiguration;
import demo.jian.com.conf.WeatherMessagesConfiguration;
import demo.jian.com.exceptions.RestfulCallException;
import demo.jian.com.model.WeatherResponse;
import demo.jian.com.model.WeatherWebResult;
import demo.jian.com.utils.WebResultDataConverterUtils;
import demo.jian.com.workers.WeatherWorker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Weather Service implementation
 */
@Slf4j
@Service("weatherService")
@EnableConfigurationProperties({SpringWeatherConfiguration.class, WeatherMessagesConfiguration.class})
@AllArgsConstructor
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    private OkHttpClient okHttpClient;

    @Autowired
    private ObjectMapper objectMapper;

    private SpringWeatherConfiguration springWeatherConfiguration;

    private WeatherMessagesConfiguration weatherMessagesConfiguration;

    /**
     *
     * @param cityName
     * @return
     */
    @Override
    public WeatherWebResult checkCityWeatherByName(String cityName) {

        WeatherWebResult.WeatherWebResultBuilder builder = WeatherWebResult.builder();

        WeatherWorker weatherWorker = initiateWorker();
        try {
            WeatherResponse weatherResponse = weatherWorker.processRequest(cityName);
            log.debug("weather is successfully received: " +  weatherResponse);
            String message =
                    String.format(
                            weatherMessagesConfiguration.getSuccessfulRetrieved(),
                            weatherResponse.getName(),
                            weatherResponse.getSys().getCountry());
            return WebResultDataConverterUtils.populateWeatherResultFromWeatherResponse(weatherResponse, message);

        } catch (IOException ioe) {
            log.error("Caught exception when retrieving ..." + cityName  + ioe.getMessage() );
            builder.message(weatherMessagesConfiguration.getDefaultErrorMsg());
        } catch (RestfulCallException restCallApiException) {

            if(restCallApiException.getResponseCode() == HttpStatus.SC_NOT_FOUND){
                builder.message(weatherMessagesConfiguration.getNoCityFound());
            }else {
                log.error("Caught exception when retrieving ..." + cityName + " with code : "+ restCallApiException.getResponseCode());
                builder.message(weatherMessagesConfiguration.getDefaultErrorMsg());
            }
        }
        return builder.build();

    }

    private WeatherWorker initiateWorker() {


        return WeatherWorker.builder()
                .springWeatherConfiguration(springWeatherConfiguration)
                .okHttpClient(okHttpClient)
                .objectMapper(objectMapper).build();
    }


}

