package demo.jian.com.services;

import demo.jian.com.model.WeatherWebResult;

/**
 * Interface for weather service
 */
public interface WeatherService {

    WeatherWebResult checkCityWeatherByName(String cityName);

}
