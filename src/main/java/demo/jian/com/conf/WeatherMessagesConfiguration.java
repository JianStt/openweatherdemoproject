
package demo.jian.com.conf;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Messages locale configuration
 */
@Data
@NoArgsConstructor
@ConfigurationProperties(prefix = "web.templates.en")
public class WeatherMessagesConfiguration {

    private String successfulRetrieved;
    private String noCityFound;
    private String defaultErrorMsg;

}
