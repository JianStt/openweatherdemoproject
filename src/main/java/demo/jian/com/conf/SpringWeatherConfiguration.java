

package demo.jian.com.conf;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * RestFul API Configuration
 */
@Data
@NoArgsConstructor
@ConfigurationProperties(prefix = "weather.conf.rest")
public class SpringWeatherConfiguration{

    private String scheme;
    private String endPointHost;
    private String endPointPath;
    private String apiKey;

}
