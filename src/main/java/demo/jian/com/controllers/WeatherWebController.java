package demo.jian.com.controllers;

import demo.jian.com.model.WeatherWebResult;
import demo.jian.com.services.WeatherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Spring web controller
 */
@AllArgsConstructor
@Controller
public class WeatherWebController {

    private WeatherService weatherService;

    /**
     * Weather portal entry point
     * @param cityName
     * @param model
     * @return
     */
    @GetMapping("/weatherByCityName")
    public String weatherByCityName(
            @RequestParam(name="cityName", required=false, defaultValue="") String cityName,
            Model model)
    {
        WeatherWebResult weatherWebResult = weatherService.checkCityWeatherByName(cityName);
        model.addAttribute("wr", weatherWebResult);
        return "weatherByCityName";
    }

}