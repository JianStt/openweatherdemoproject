package demo.jian.com.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class WeatherWebResult {
    @Builder.Default
    private String message;
    @Builder.Default
    private String todayDate;
    @Builder.Default
    private String cityName;
    @Builder.Default
    private String overallDescription;
    @Builder.Default
    private String temperatureDisplayInFahrenheit;
    @Builder.Default
    private String temperatureDisplayInCelsius;

    @Builder.Default
    private String sunRiseInTwleveHoursFormat;

    @Builder.Default
    private String sunSetInTwleveHoursFormat;


}
