
package demo.jian.com.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Main {

    private Long humidity;
    private Long pressure;
    private Double temp;
    @JsonProperty("temp_max")
    private Double tempMax;
    @JsonProperty("temp_min")
    private Double tempMin;

}
