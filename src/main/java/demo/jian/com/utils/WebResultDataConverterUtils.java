package demo.jian.com.utils;

import demo.jian.com.model.WeatherResponse;
import demo.jian.com.model.WeatherWebResult;
import lombok.NonNull;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

/**
 * Utils for converting restFUL API response into Web Display POJO
 */
public class WebResultDataConverterUtils {

    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
    public static final SimpleDateFormat simple12HoursFormat = new SimpleDateFormat("hh:mm:ss z");
    public static final Double ABSOLUTE_TEMP = 273.15;
    public static final NumberFormat numberFormat = new DecimalFormat("#0.00");
    /**
     * private constructor since it is a static class
     */
    private WebResultDataConverterUtils() {};


    /**
     *
     * @param weatherResponse
     * @param message
     * @return
     */
    public static WeatherWebResult populateWeatherResultFromWeatherResponse(
            @NonNull WeatherResponse weatherResponse, String message){
        WeatherWebResult.WeatherWebResultBuilder builder = WeatherWebResult.builder();
        builder.message(message);


        builder.cityName(weatherResponse.getName() + "," + weatherResponse.getSys().getCountry());

        StringBuilder overall = new StringBuilder();
        weatherResponse.getWeather().stream().forEach(weather->overall.append(weather.getMain() + "--" + weather.getDescription() + ";" ));
        builder.overallDescription(overall.toString());

        builder.todayDate(UtcTimeConverter.convertUtcUnixSeconds(weatherResponse.getDt()));

        builder.sunRiseInTwleveHoursFormat("Local time: " +
                UtcTimeConverter.convertUtcUnixSecondsLocalTime(
                        weatherResponse.getSys().getSunrise(),
                        weatherResponse.getTimezone()
                        ));

        builder.sunSetInTwleveHoursFormat( "Local time: " +
                UtcTimeConverter.convertUtcUnixSecondsLocalTime(
                        weatherResponse.getSys().getSunset(),
                        weatherResponse.getTimezone()));

        double celsiusTemp = weatherResponse.getMain().getTemp() - ABSOLUTE_TEMP;
        double fabrenheitTemp = celsiusTemp * 9 / 5 + 32;
        builder.temperatureDisplayInCelsius(numberFormat.format(celsiusTemp) + " C");
        builder.temperatureDisplayInFahrenheit(numberFormat.format(fabrenheitTemp) + " F");

        return builder.build();
    }

}
