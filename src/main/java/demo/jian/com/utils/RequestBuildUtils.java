package demo.jian.com.utils;

import demo.jian.com.conf.SpringWeatherConfiguration;
import demo.jian.com.constants.WeatherConstants;
import lombok.NonNull;
import okhttp3.Request;
import org.apache.http.client.utils.URIBuilder;

public class RequestBuildUtils {

    public static Request buildHttpRequest(@NonNull String cityName, SpringWeatherConfiguration springWeatherConfiguration) {

        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(springWeatherConfiguration.getScheme());
        uriBuilder.setHost(springWeatherConfiguration.getEndPointHost());
        uriBuilder.setPath(springWeatherConfiguration.getEndPointPath());

        uriBuilder.addParameter(WeatherConstants.WEATHER_URL_QUERY_CITY_NAME_KEY, cityName);
        uriBuilder.addParameter(WeatherConstants.WEATHER_URL_QUERY_APPID_KEY, springWeatherConfiguration.getApiKey());

        Request.Builder requestBuilder = new Request.Builder()
                .url(uriBuilder.toString());

        return requestBuilder.build();
    }

}
