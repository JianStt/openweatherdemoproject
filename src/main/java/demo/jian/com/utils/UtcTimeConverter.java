package demo.jian.com.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

/**
 * UTC timezone converter utils
 */
public class UtcTimeConverter {

    public static final long ONE_HOUR_SECONDS = 3600;


    /**
     *
     * @param unixSeconds
     * @return
     */
    public static String convertUtcUnixSeconds(long unixSeconds){
        Date date = new Date(unixSeconds*1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        jdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return jdf.format(date);
    }

    /**
     *
     * @param unixSeconds
     * @param shiftedSeconds
     * @return
     */
    public static String convertUtcUnixSecondsLocalTime(long unixSeconds, long shiftedSeconds){

        Instant instant = Instant.ofEpochSecond(unixSeconds);
        ZoneOffset zoneOffSet = ZoneOffset.of(timeZoneOffSetComputer(shiftedSeconds));
        OffsetDateTime sth = instant.atOffset(zoneOffSet);
        DateTimeFormatter jdf = DateTimeFormatter.ofPattern("HH:mm a");
        String result = sth.format(jdf).replaceAll("\\.","");
        return result;
    }

    /**
     *
     * @param shiftedSeconds
     * @return
     */
    public static String timeZoneOffSetComputer(long shiftedSeconds){
        if(shiftedSeconds < - 12 * ONE_HOUR_SECONDS || shiftedSeconds > 12 * ONE_HOUR_SECONDS){
            return "00:00";
        }
        int timeZoneOffSet= (int)(shiftedSeconds/ONE_HOUR_SECONDS);

        String unsigned = StringUtils.leftPad(Math.abs(timeZoneOffSet)+"", 2 , '0') + ":00";

        return timeZoneOffSet < 0 ? "-" + unsigned : "+" + unsigned;
    }

}
