package demo.jian.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot Main
 */
@SpringBootApplication
public class WeatherApplication {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(WeatherApplication.class, args);
    }

}