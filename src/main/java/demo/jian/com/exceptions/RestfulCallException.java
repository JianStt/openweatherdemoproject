package demo.jian.com.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * RestFul Call Exception
 */
@Data
@AllArgsConstructor
public class RestfulCallException extends Exception {

    private String errorMsg;
    private int responseCode;
}
